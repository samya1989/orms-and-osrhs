path='/home/samya/Desktop/Results/Testing_Ducks/';
writepath='/home/samya/Desktop/Results/100Ducks/';
 
imagefiles = [dir(strcat(path,'*.ppm')) ; dir(strcat(path,'*.gif'));  dir(strcat(path,'*.jpg')); dir(strcat(path,'*.png'))];
% Number of files found
filescount = length(imagefiles)  

for go=1:filescount
        
        currentfilename = imagefiles(go).name;
        currentimage = imread(strcat(path,currentfilename));
        imwrite(currentimage,strcat(writepath,num2str(go),'.jpg'),'jpg');
        fprintf('\n%d %s done',go,currentfilename);
end