function [] = trim()

    path='/home/samya/Desktop/eagle/';
    files=dir(strcat(path,'*.ppm'));
    
    for i=1:length(files)
        
        I=imread(strcat(path,files(i).name));
        
        I=rgb2gray(I);
        I=(I~=255);
        I( ~any(I,2), : ) = [];
        I( :, ~any(I,1) ) = [];
        I=(I==0);
        I=I*255;
        imwrite(I,strcat(path,files(i).name));
        
    end
    
end

