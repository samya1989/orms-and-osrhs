   %%%%%%%%%%%%%%%%%%%%%%%%%% START TESTING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   clear all;
   close all;
   clc;
      
   objectid=1;
   load('/home/samya/Desktop/Results/Duck/Pose1/MMCF.mat','MMCF');
   load('/home/samya/Desktop/Results/Duck/Pose1/Svm.mat','model');
   
   % addpath to the libsvm toolbox
   addpath('/home/samya/Desktop/libsvm-3.17/matlab');
   
   [filterlength filterwidth]=size(MMCF);
      
   testimagelength=300;
   testimagewidth=400;
   % Parameter to judge closeness of objects
   overlapthreshold=0;
   
   filename='3.jpg';
   imagepath=strcat('/home/samya/Desktop/Results/100Ducks/',filename);
   %%%%%%%%%%%%%%%%%%%%%%% TEST IMAGE PRE-PROCESSING %%%%%%%%%%%%%%%%%%%%%
    testim=imread(imagepath);
    colorim=imread(imagepath);
   
    testim=imresize(testim,[testimagelength testimagewidth]);
    colorim=imresize(colorim,[testimagelength testimagewidth]);
    testimcolor=testim;
    
    figure('Name','Input Test Image');imshow(testim);
    
    [rows columns numberOfColorChannels] = size(testim);
    
    if (numberOfColorChannels > 1)
        testim=rgb2gray(testim);
    end
    
        
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
       
   %%%%%%%%%%%%%%%%%%%%%%%%    TESTing Starts    %%%%%%%%%%%%%%%%%%%%%%%%%%

   atscale=double(zeros(100,1));
   coordinates=double(zeros(100,2));
   surity=double(zeros(100,1));
   
   count=0;
   totalcount=0;
   
   for zoom=120:-1:50
       
       resize=zoom/100;
       fprintf('\n%s %d %s\n','Searching at scale:',zoom,'%');
       
       testimdouble=imresize(testim,resize);
       [testimdoublelength testimdoublewidth]=size(testimdouble);
       
       imgnottransformed=testimdouble;
       testimdouble=double(testimdouble);
       
       
       %Making the test image zero mean and unit variance
       mean_val = mean(testimdouble(:));
       variance_val = var(testimdouble(:));
       
       if variance_val~=0
            testimdouble = (testimdouble-mean_val)/variance_val;
       end
       
       %Making the test image unit norm
       testimdouble = testimdouble/norm(testimdouble(:),2);
      
       result=xcorr2(testimdouble,MMCF);
       %figure;surf(result); 
       
       % Getting Local Maximas
       dilate=ones(filterlength-1,filterwidth-1);
       dilate(floor(filterlength/2),floor(filterwidth/2))=0;
       maximas = result > imdilate(result,dilate);

       
       % For each local maximas checking if the object is there
       
       [m n]=size(result);
       
       for i=1:m
           for j=1:n

               if(maximas(i,j)==1)
                   
                       % Now check if the image corresponding to the peak
                       % is in the class
                       
                       if((i-filterlength+1>0) & (j-filterwidth+1>0) & i<=testimdoublelength & j<=testimdoublewidth)
                            
                            subimg=imgnottransformed(i-filterlength+1:i,j-filterwidth+1:j);
                            
                            subimg=double(subimg);
                                  
                            %Making the test image zero mean and unit variance
                            mean_val = mean(subimg(:));
                            variance_val = var(subimg(:));
       
                            if variance_val~=0
                                subimg = (subimg-mean_val)/variance_val;
                            end
       
                            %Making the test image unit norm
                            subimg = subimg/norm(subimg(:),2);
                            
                            csvwrite('temp.csv',subimg(:).');
                            system('./convert.out temp.csv');
                            
                            % read the test data set
                            [datalabel, testdata] = libsvmread(fullfile(pwd,'temp.csv_out'));
                            
                            [predict_label, accuracy, prob_values] = svmpredict([1], testdata, model, '-b 1');
                            
                            if(predict_label==1)
                                
                                totalcount=totalcount+1;
                                maxoverlap=0;                                
                                for k=1:count
                                    
                                    rectanglecurrent=floor([i-filterlength+1 j-filterwidth+1  filterlength filterwidth]*(atscale(k)/resize));
                                    rectanglestored=[coordinates(k,1)-filterlength+1 coordinates(k,2)-filterwidth+1 filterlength filterwidth];
                                    totalarea=(filterlength*filterwidth)*(1 + (atscale(k)/resize)^2)-rectint(rectanglecurrent,rectanglestored);
                                    overlappercent=(rectint(rectanglecurrent,rectanglestored)/totalarea)*100;
                                    
                                    if overlappercent > maxoverlap
                                    
                                       maxoverlap=overlappercent;
                                       minpos=k;
                                        
                                    end
                                end   
                                  
                                flag=0;
                                
                                if(maxoverlap>overlapthreshold)   
                                    flag=1;
                                    if prob_values(1)>surity(minpos)
                                        
                                            %helloo='om'
                                            fprintf('--> Object found and Updated\n');
                                            surity(minpos)=prob_values(1);
                                            coordinates(minpos,1)=i;
                                            coordinates(minpos,2)=j;
                                            atscale(minpos)=resize;
                                            
                                    end
                                    
                                end
                                
                                if flag==0
                                    
                                    count=count+1;
                                    surity(count)=prob_values(1);
                                    coordinates(count,1)=i;
                                    coordinates(count,2)=j;
                                    atscale(count)=resize;
                                                                   
                                end
                                
                            end
                       end
               end

           end
       end
       
%          surity(1:count).'
%          coordinates(1:count,:)
%          atscale(1:count)
       fprintf('Total SVM positives=%d, Objects found=%d\n',totalcount,count);
   
       
   end
   
   objects=zeros(count,4);
   for i=1:count
       
       objects(i,1)=floor(coordinates(i,1)/atscale(i));
       objects(i,2)=floor(coordinates(i,2)/atscale(i));
       objects(i,3)=floor(filterlength/atscale(i));
       objects(i,4)=floor(filterwidth/atscale(i));
       
   end
      
%    for i=1:count
%        
%        tempim=testimcolor;
%        tempim=imresize(tempim,atscale(i));
%        subimg=tempim(coordinates(i,1)-filterlength+1:coordinates(i,1),coordinates(i,2)-filterwidth+1:coordinates(i,2),:);
%        figurename=strcat('Found at x=',num2str(coordinates(i,1)-filterlength+1),':',num2str(coordinates(i,1)),{' '},'y=',num2str(coordinates(i,2)-filterwidth+1),':',num2str(coordinates(i,2)));
%        subimg=imresize(subimg,2.75);
%        figure('Name',char(figurename));imshow(subimg);
%    end
   
   tempim=testimcolor;
   shapeInserter = vision.ShapeInserter('BorderColor','Custom','CustomBorderColor', uint8([255 0 0]));
   for i=1:count
       
       x=(coordinates(i,1)-filterlength+1);
       y=(coordinates(i,2)-filterwidth+1);
       x=floor(x/atscale(i));
       y=floor(y/atscale(i));
       length=floor(filterlength/atscale(i));
       width=floor(filterwidth/atscale(i));
       rectangle = int32([x y length width]);
       tempim= step(shapeInserter, tempim, rectangle);

   end
   
   
   if count>0  
        figure('Name','Found objects in Red Rectangles');imshow(tempim);
        %fprintf('\n ---Starting Background Prediction---\n\n');
        %shapeless(colorim,objects,objectid);
        imwrite(tempim,strcat('/home/samya/Desktop/Results/100DucksResults/',filename));
        %imwrite(colorim,'/home/samya/Desktop/Image_Pasting/ORMSWB/6/input.jpg');
   end
   
   