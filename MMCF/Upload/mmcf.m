function [] = mmcf()

%     Get list of all files in this directory
%     DIR returns as a structure array.  You will need to use () and . to get
%     the file names.
    
%     positivepath='/home/samya/Desktop/MMCF/test1/';    %Takes file who are in the class i.e. 1
%     negativepath='/home/samya/Desktop/MMCF/test2/';    %Takes file who are not in the class i.e. -1
%     
%     filterlength=40;
%     filterwidth=70;
%     paddedfilterlength=(2*filterlength*filterwidth-1);
%     
%     positiveimagefiles = [dir(strcat(positivepath,'*.jpg')) ; dir(strcat(positivepath,'*.gif'))];
%     % Number of positive files found
%     positivefilescount = length(positiveimagefiles)    
%     
%     negativeimagefiles = [dir(strcat(negativepath,'*.jpg'));  dir(strcat(negativepath,'*.gif'))];
%     % Number of negative files found
%     negativefilescount = length(negativeimagefiles)    
%     
%     allimagefiles = [positiveimagefiles ; negativeimagefiles];
%     totalfilescount = length(allimagefiles)
%     
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     
%     fprintf('Done till image counting\n');
%     
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     
%     
%     lambda=0.9;     
%     D=vpa(zeros(paddedfilterlength,1),2);
%     
%     fftimagevectors=zeros(totalfilescount,paddedfilterlength);
%     
%     for go=1:totalfilescount
%         
%         if(go<=positivefilescount)
%             path=positivepath;
%         else
%             path=negativepath;
%         end
%         
%         currentfilename = allimagefiles(go).name;
%         currentimage = imread(strcat(path,currentfilename));
%         currentimage = imresize(currentimage,[filterlength filterwidth]);
%         
%         [rows columns numberOfColorChannels] = size(currentimage);
%         if numberOfColorChannels > 1
%             currentimage=rgb2gray(currentimage);
%         end
%         
%         currentimage=double(currentimage);
%         currentimage=currentimage(:);
%         currentimage=padarray(currentimage,[paddedfilterlength-length(currentimage) 0],'post');
%         currentimage=myfft(currentimage);
%         currentimage=currentimage.';
%         
%         D = D + (currentimage).*conj(currentimage);
%         
%         fftimagevectors(go,:) = currentimage.';
%                   
%     end
%     
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     
%     fprintf('Done till image reading and pre processing\n');
%     
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     
%     
%     % This is Eq. 22
%     S=( ( 1-lambda ) * D ) + lambda * ones( paddedfilterlength,1 );
%     S=vpa(S,2);
%         
%     Sinv=1./S;
%     Sinvhalf=sqrt(Sinv);
%     
%     %Assigning classes to the images
%     targetclass = zeros(1, totalfilescount);
%     resultclass = zeros(1, totalfilescount);
%     
%     for go=1:totalfilescount
%         
%         targetclass(1,go)=1;
%         resultclass(1,go)=1;
%         
%         if(go>positivefilescount)
%             
%             targetclass(1,go)=-1;     % t_i in paper
%             resultclass(1,go)=0;      % c_i in paper
%             
%         end
%         
%     end
%     
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     
%     fprintf('Done till Calculating S and setting Class Values for Training\n');
%     
%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     
%     
%     fftimagevectors=vpa(fftimagevectors,2);  %For high precision
%     
%     H=ctranspose(fftimagevectors.')*diag(Sinv)*(fftimagevectors.');
%     H=diag(targetclass)*H*diag(targetclass);
%     
%     H=real(H);   % Elimination of 0.0000i
%     H=double(H); % Back to normal precision
%     
%    %%%%%%%%%%%%%%%%%%%%% TRAINING & CALCULATING FILTER %%%%%%%%%%%%%%%%%%%%
%    
%    options = optimset('Algorithm','interior-point-convex','TolFun',10^-16,'TolX',10^-16,'MaxIter',10000);   
%    alpha=quadprog(2*H,(-1)*resultclass.',[],[],targetclass,zeros(1,1),zeros(totalfilescount,1),lambda*ones(totalfilescount,1),[],options)
%    
%    MMCF=diag(Sinvhalf)*transpose(fftimagevectors*diag(Sinvhalf))*alpha;
%    MMCF=double(MMCF);
%    MMCF=myifft(MMCF);
%    MMCF=MMCF(1:(filterlength*filterwidth));
%    MMCF=real(MMCF);
%    MMCF=transpose(MMCF);
%    MMCF=vec2mat(MMCF,filterlength).';
%    
%    save('/home/samya/Desktop/just_test.mat');
%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     
%    fprintf('Training Over \n');
    
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   %%%%%%%%%%%%%%%%%%%%%%%%%% START TESTING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   load('/home/samya/Desktop/just_test.mat');
   
   % Set desired test image length and breadth	
   testimagelength = 300;
   testimagewidth = 400;
   
   %%%%%%%%%%%%%%%%%%%%%%% TEST IMAGE PRE-PROCESSING %%%%%%%%%%%%%%%%%%%%%
   
   testim=imread('/home/samya/Desktop/test.jpg');
   
   [rows columns numberOfColorChannels] = size(testim);
    
   if (numberOfColorChannels > 1)
       testim=rgb2gray(testim);
   end
      
   testim=imresize(testim,[testimagelength testimagewidth]);
   testim=double(testim);

   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
   %%%%%%%%%%%%%%%%%%%%%%%%    TESTing Starts    %%%%%%%%%%%%%%%%%%%%%%%%%

   result=xcorr2(MMCF,testim);
   surf(result);
       
end
