function [ x ] = myifft(X)

%myifft is opposite of myfft

%   The inverse myfft (computed by myifft) is given by
%                    N
%      x(n) = (1/N) sum  X(k)*exp( j*2*pi*(k-1)*(n-1)/N), 1 <= n <= N.
%                   k=1

    N=length(X);
    x=zeros(N,1);
    
    for n=1:N
        
        result=0;
        
        for k=1:N
            transform=n-(N-1)/2;
            result=result+X(k)*exp(j*2*pi*(transform-1)*(k-1)/N);
            
        end
        
        x(n)=result/N;
    end

end