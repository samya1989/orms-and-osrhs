import java.io.File;
import java.io.FileWriter;
import javax.script.*;

public class ColorName {
    public String getColorName(String colorcode) throws Exception {
        
        colorcode="#"+colorcode;
        // create a script engine manager
        ScriptEngineManager factory = new ScriptEngineManager();
        // create JavaScript engine
        ScriptEngine engine = factory.getEngineByName("JavaScript");
        // evaluate JavaScript code from given file - specified by first argument
        engine.eval(new java.io.FileReader("ntc.js"));
        Invocable inv = (Invocable) engine;
        
        // get script object on which we want to call the method
        Object obj = engine.get("ntc");
        // invoke the method named "name" on the script object "obj"
        inv.invokeMethod(obj, "name", colorcode );
        Object o=engine.get("shade");
            
        return o.toString();

    }
    
    public static void main(String[] args) throws Exception {
        (new ColorName()).getColorName("ffffff");
    }
}

