function [ done ] = train_mmcf()

%     Get list of all files in this directory
%     DIR returns as a structure array.  You will need to use () and . to get
%     the file names.
    
    positivepath='/home/samya/Desktop/Thesis_ODD/Duck/Pose5/';         %Takes file who are in the class i.e. 1
    negativepath='/home/samya/Desktop/Thesis_ODD/Backgrounds/';         %Takes file who are not in the class i.e. -1
    
    filterlength=60;
    filterwidth=80;
    paddedfilterlength=(2*filterlength*filterwidth-1);
    
    positiveimagefiles = [dir(strcat(positivepath,'*.ppm')) ; dir(strcat(positivepath,'*.gif'));  dir(strcat(positivepath,'*.jpg')); dir(strcat(positivepath,'*.png'))];
    % Number of positive files found
    positivefilescount = length(positiveimagefiles)    
    
    negativeimagefiles = [dir(strcat(negativepath,'*.ppm'));  dir(strcat(negativepath,'*.gif'));  dir(strcat(negativepath,'*.jpg')); dir(strcat(negativepath,'*.png'))];
    % Number of negative files found
    negativefilescount = length(negativeimagefiles)    
    
    allimagefiles = [positiveimagefiles ; negativeimagefiles];
    totalfilescount = length(allimagefiles)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    helloo='Done till image counting'
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    lambda=0.6;     
    D=vpa(zeros(paddedfilterlength,1),2);
    
    fftimagevectors=zeros(totalfilescount,paddedfilterlength);
    imagevectors=zeros(totalfilescount,filterlength*filterwidth);
    
    for go=1:totalfilescount
        
        if(go<=positivefilescount)
            path=positivepath;
        else
            path=negativepath;
        end
        
        currentfilename = allimagefiles(go).name;
        currentimage = imread(strcat(path,currentfilename));
        currentimage = imresize(currentimage,[filterlength filterwidth]);
        
        [rows columns numberOfColorChannels] = size(currentimage);
        if numberOfColorChannels > 1
            currentimage=rgb2gray(currentimage);
        end
        
        currentimage=double(currentimage);
        currentimage=currentimage(:);
        
        %Making it zero mean and unit variance
        mean_val = mean(currentimage);
        variance_val = var(currentimage);
        
        if variance_val~=0
            currentimage = (currentimage-mean_val)/variance_val;
        end
        
        %Making the image unit norm
        currentimage=currentimage/norm(currentimage,2);
        
        imagevectors(go,:)=currentimage.';
        
        currentimage=padarray(currentimage,[paddedfilterlength-length(currentimage) 0],'post');
        currentimage=myfft(currentimage);
        currentimage=currentimage.';
        
        D = D + (currentimage).*conj(currentimage);
        
        fftimagevectors(go,:) = currentimage.';
                  
    end
    
%     positives=imagevectors(1:positivefilescount,:);
%     negatives=imagevectors(positivefilescount+1:end,:);
%     
%     csvwrite('/home/samya/Desktop/P_Testing/eagle.csv',positives);
%     csvwrite('/home/samya/Desktop/P_Testing/background.csv',negatives);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    helloo='Done till image reading and pre processing'
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    % This is Eq. 22
    S=( ( 1-lambda ) * D ) + lambda * ones( paddedfilterlength,1 );
    S=vpa(S,2);
        
    Sinv=1./S;
    Sinvhalf=sqrt(Sinv);
    
        
    %Assigning classes to the images
    targetclass = zeros(1, totalfilescount);
    resultclass = zeros(1, totalfilescount);
    
    for go=1:totalfilescount
        
        targetclass(1,go)=1;
        resultclass(1,go)=1;
        
        if(go>positivefilescount)
            
            targetclass(1,go)=-1;     % t_i in paper
            resultclass(1,go)=0;      % c_i in paper
            
        end
        
    end
    
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    helloo='Done till Calculating S and setting Class Values for Training'
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    fftimagevectors=vpa(fftimagevectors,2);  %For high precision
    
    H=ctranspose(fftimagevectors.')*diag(Sinv)*(fftimagevectors.');
    H=diag(targetclass)*H*diag(targetclass);
    
    H=real(H);   % Elimination of 0.0000i
    H=double(H); % Back to normal precision
    
   %%%%%%%%%%%%%%%%%%%%% TRAINING & CALCULATING FILTER %%%%%%%%%%%%%%%%%%%%
   
   % C or boxconstraint value has to be derived by grid search
   C=4;
   
   options = optimset('Algorithm','interior-point-convex','TolFun',10^-16,'TolX',10^-16,'MaxIter',10000);   
   alpha=quadprog(2*H,(-1)*resultclass.',[],[],targetclass,zeros(1,1),zeros(totalfilescount,1),C*lambda*ones(totalfilescount,1),[],options)
   
   MMCF=diag(Sinvhalf)*transpose(fftimagevectors*diag(Sinvhalf))*alpha;
   MMCF=double(MMCF);
   MMCF=myifft(MMCF);
   MMCF=MMCF(1:(filterlength*filterwidth));
   MMCF=real(MMCF);
   MMCF=transpose(MMCF);
   MMCF=vec2mat(MMCF,filterlength).';
   
   %T=svmtrain(imagevectors,targetclass.');
       
   save('/home/samya/Desktop/Thesis_ODD/Duck/Pose5/MMCF.mat','MMCF');
   %save('/home/samya/Desktop/just_eagle.mat');
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    helloo='Done training'
    
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   
    
   %%%%%%%%%%%%%%%%%%%%%%%%%% START TESTING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%    load('/home/samya/Desktop/just_eagle.mat');
%    load('/home/samya/Desktop/P_Testing/T_eagle.mat','T_Eagle');
%       
%    testimagelength=300;
%    testimagewidth=400;
%    
%    %%%%%%%%%%%%%%%%%%%%%%% TEST IMAGE PRE-PROCESSING %%%%%%%%%%%%%%%%%%%%%
%    testim=imread('/home/samya/Desktop/Thesis_ODD/Eagle/Pose5_Front/eagle0085.ppm');
%    
%     [rows columns numberOfColorChannels] = size(testim);
%     
%     if (numberOfColorChannels > 1)
%         testim=rgb2gray(testim);
%     end
%       
%     testim=imresize(testim,[testimagelength testimagewidth]);
%     figure;imshow(testim);
%     
%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     
%        
%    %%%%%%%%%%%%%%%%%%%%%%%%    TESTing Starts    %%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%    for zoom=100:-1:20
%        
%        resize=zoom/100;
%        testimdouble=imresize(testim,resize);
%        [testimdoublelength testimdoublewidth]=size(testimdouble);
%        
%        imgnottransformed=testimdouble;
%        testimdouble=double(testimdouble);
%        
%        
%        %Making the test image zero mean and unit variance
%        mean_val = mean(testimdouble(:));
%        variance_val = var(testimdouble(:));
%        
%        if variance_val~=0
%             testimdouble = (testimdouble-mean_val)/variance_val;
%        end
%        
%        %Making the test image unit norm
%        testimdouble = testimdouble/norm(testimdouble(:),2);
%       
%        result=xcorr2(testimdouble,MMCF);
%        %figure;surf(result); 
%        
%        % Getting Local Maximas
%        dilate=ones(2*filterlength-1,2*filterwidth-1);
%        dilate(filterlength,filterwidth)=0;
%        maximas = result > imdilate(result,dilate);
% 
%        
%        % For each local maximas checking if the object is there
%        
%        [m n]=size(result);
%        
%        for i=1:m
%            for j=1:n
% 
%                if(maximas(i,j)==1)
%                    
%                        % Now check if the image corresponding to the peak
%                        % is in the class
%                        
%                        if((i-filterlength+1>0) & (j-filterwidth+1>0) & i<=testimdoublelength & j<=testimdoublewidth)
%                             
%                             subimg=testimdouble(i-filterlength+1:i,j-filterwidth+1:j);
%                             
%                             subimg=double(subimg);
%                                   
%                             %Making the test image zero mean and unit variance
%                             mean_val = mean(subimg(:));
%                             variance_val = var(subimg(:));
%        
%                             if variance_val~=0
%                                 subimg = (subimg-mean_val)/variance_val;
%                             end
%        
%                             %Making the test image unit norm
%                             subimg = subimg/norm(subimg(:),2);
%                                                         
%                             if(svmclassify(T_Eagle,(subimg(:).'))==1)
%                                 
%                                 subimg=imgnottransformed(i-filterlength+1:i,j-filterwidth+1:j);
%                                 figure;imshow(subimg);
%                                 
%                             end
%                        end
%                end
% 
%            end
%        end
%        
%    end
end