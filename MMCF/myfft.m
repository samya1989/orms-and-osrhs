function[X] = myfft(x)

% This function implements fft from the centre i.e. -N/2 to N/2


%                    N
%      X(k) =       sum  x(n)*exp(-j*2*pi*(k-1)*(n-1)/N), 1 <= k <= N.
%                   n=1

    N=length(x);

    X=zeros(1,N);
    
    for k=1:N
        
        result=0;
        
        for n=1:N
            transform=n-(N-1)/2;
            result=result+x(n)*exp(-j*2*pi*(k-1)*(transform-1)/N);
            
        end
        
        X(k)=result;
    end
end