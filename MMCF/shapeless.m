function[]=shapeless(image,objects,objectid)

presentpath='/home/samya/Desktop/BSR/grouping/';
addpath(fullfile(presentpath,'lib'));

%% Compute globalPb and hierarchical segmentation for an image.

I=image;
[imagelength imagewidth channelcount]=size(image);
%  I=imread(strcat(presentpath,'data/test1.jpg'));
%  I=imresize(I,[300 400]);   
imwrite(I,strcat(presentpath,'data/wall.jpg'));
%figure('Name','Test Color Image');imshow(strcat(presentpath,'data/wall.jpg'));

%% 1. compute globalPb on a BSDS image (5Gb of RAM required)
%clear all; close all; clc;

%presentpath='/home/samya/Desktop/BSR/grouping/';

imgFile = strcat(presentpath,'data/wall.jpg');
outFile = strcat(presentpath,'data/wall_gPb.mat');

gPb_orient = globalPb(imgFile, outFile);


%% 2. compute Hierarchical Regions

% for boundaries
ucm = contours2ucm(gPb_orient, 'imageSize');
imwrite(ucm,strcat(presentpath,'data/wall_ucm.bmp'));

% for regions 
ucm2 = contours2ucm(gPb_orient, 'doubleSize');
save(strcat(presentpath,'data/wall_ucm2.mat'),'ucm2');

%% 3. usage example
%clear all;close all;clc;

%load double sized ucm
load(strcat(presentpath,'data/wall_ucm2.mat'),'ucm2');

% convert ucm to the size of the original image
ucm = ucm2(3:2:end, 3:2:end);

% get the boundaries of segmentation at scale k in range [0 1]
k = 0.3;
bdry = (ucm >= k);

% get superpixels at scale k without boundaries, gives connected regions,
% will be used for shape matching

[labels2 uniformregioncount] = bwlabel(ucm2 <= k);
labels = labels2(2:2:end, 2:2:end);

%uniformregioncount has the number of connected components
%connectedregions=uniformregioncount  

%Fill up with a DEAD value where objects have been found
deadvalue=uniformregioncount+1;

[objectcount features]=size(objects);
for i=1:objectcount
    labels(objects(i,1)-objects(i,3)+1:objects(i,1),objects(i,2)-objects(i,4)+1:objects(i,2))=deadvalue;
end

pixelsums=zeros(uniformregioncount,3);
pixelcountperregion=zeros(uniformregioncount,1);

%select each region and sum up color values 
for i=1:300
    for j=1:400
    
    if labels(i,j)==deadvalue
        continue;
    end
        
    pixelcountperregion(labels(i,j),1)=pixelcountperregion(labels(i,j),1)+1;
    
    for k=1:3
        
        pixelsums(labels(i,j),k)=pixelsums(labels(i,j),k)+double(I(i,j,k))/255;
        
    end
    
    end
end

pixelsavg=zeros(uniformregioncount,3);
%for each region find average
for i=1:uniformregioncount
    
    if(pixelcountperregion(i)<100)
        %Setting it to black as there is no pixel or count is very less
        pixelsavg(i,:)=0;
        continue;
    end
    
    for j=1:3
        
        pixelsavg(i,j)=pixelsums(i,j)/pixelcountperregion(i,1);
        pixelsavg(i,j)=pixelsavg(i,j)*255;
    end
    
end 

pixelsavg=floor(pixelsavg);

fprintf('\n');

RedFlag=0;
OrangeFlag=0;
YellowFlag=0;
GreenFlag=0;
BlueFlag=0;
VioletFlag=0;
BrownFlag=0;
BlackFlag=0;
GreyFlag=0;
WhiteFlag=0;

Redim=double(zeros(imagelength,imagewidth,3));
Orangeim=double(zeros(imagelength,imagewidth,3));
Yellowim=double(zeros(imagelength,imagewidth,3));
Greenim=double(zeros(imagelength,imagewidth,3));
Blueim=double(zeros(imagelength,imagewidth,3));
Violetim=double(zeros(imagelength,imagewidth,3));
Brownim=double(zeros(imagelength,imagewidth,3));
Blackim=double(zeros(imagelength,imagewidth,3));
Greyim=double(zeros(imagelength,imagewidth,3));
Whiteim=double(zeros(imagelength,imagewidth,3));

tempim=double(image);

for i=1:uniformregioncount
    
        if(pixelcountperregion(i)<100)
            continue;
        end
    
        r=pixelsavg(i,1);
        g=pixelsavg(i,2);
        b=pixelsavg(i,3);
        
        if r<16
            r=dec2hex(r);
            r=strcat('0',r);
        else
            r=dec2hex(r);
        end
                
        if g<16
            g=dec2hex(g);
            g=strcat('0',g);
        else
            g=dec2hex(g);
        end
                
        if b<16
            b=dec2hex(b);
            b=strcat('0',b);
        else
            b=dec2hex(b);
        end
                
        colorcode=strcat('#',r,g,b);
        colorshade=colorname(colorcode);
        
        if(strcmp(colorshade,'Red')==1)

            RedFlag=1;

            for j=1:3
                
                Redim(:,:,j)=Redim(:,:,j) + tempim(:,:,j).*(labels==i);
                
            end

        end

        if(strcmp(colorshade,'Orange')==1)
        
            OrangeFlag=1;
	
            for j=1:3
                
                Orangeim(:,:,j)=Orangeim(:,:,j)+tempim(:,:,j).*(labels==i);
                
            end
	
        end

        if(strcmp(colorshade,'Yellow')==1)
        
            YellowFlag=1;

            for j=1:3
                
                Yellowim(:,:,j)=Yellowim(:,:,j)+tempim(:,:,j).*(labels==i);
                
            end

        end

        if(strcmp(colorshade,'Green')==1)
	
            GreenFlag=1;
	
            for j=1:3
                
                Greenim(:,:,j)=Greenim(:,:,j)+tempim(:,:,j).*(labels==i);
                
            end
        end

        if(strcmp(colorshade,'Blue')==1)
	
            BlueFlag=1;
            
            for j=1:3
                
                Blueim(:,:,j)=Blueim(:,:,j)+(tempim(:,:,j).*(labels==i));
                
            end
        end

        if(strcmp(colorshade,'Violet')==1)
	
            VioletFlag=1;
	
            for j=1:3
                
                Violetim(:,:,j)=Violetim(:,:,j)+tempim(:,:,j).*(labels==i);
                
            end
        end

        if(strcmp(colorshade,'Brown')==1)
	
            BrownFlag=1;
	
            for j=1:3
                
                Brownim(:,:,j)=Brownim(:,:,j)+tempim(:,:,j).*(labels==i);
                
            end	
        end

        if(strcmp(colorshade,'Black')==1)
        
            BlackFlag=1;
        
            for j=1:3
                
                Blackim(:,:,j)=Blackim(:,:,j)+tempim(:,:,j).*(labels==i);
            end
        end

        if(strcmp(colorshade,'Grey')==1)
	
            GreyFlag=1;
	
            for j=1:3
                
                Greyim(:,:,j)=Greyim(:,:,j)+tempim(:,:,j).*(labels==i);
              
            end

        end

        if(strcmp(colorshade,'White')==1)
        
            WhiteFlag=1;
        
            for j=1:3
                
                Whiteim(:,:,j)=Whiteim(:,:,j)+tempim(:,:,j).*(labels==i);
                
            end
        end
        
        predictedobject=predictshapelessname(objectid,colorshade);
        fprintf('%s %d  %s %s %s %s %s %s\n','Shapeless region ',i,', Color Average:',char(colorcode),', Color Shade:',char(colorshade),', Predicted Background Object:',char(predictedobject));
                
end  

if(RedFlag==1)
	colorshade='Red';
	predictedobject=predictshapelessname(objectid,colorshade);
	figure('Name',char(strcat(predictedobject,':',char(colorshade))));imshow(uint8(Redim));
end

if(OrangeFlag==1)
	colorshade='Orange';
	predictedobject=predictshapelessname(objectid,colorshade);
	figure('Name',char(strcat(predictedobject,':',char(colorshade))));imshow(uint8(Orangeim));
end

if(YellowFlag==1)
	colorshade='Yellow';
	predictedobject=predictshapelessname(objectid,colorshade);
	figure('Name',char(strcat(predictedobject,':',char(colorshade))));imshow(uint8(Yellowim));
    %imwrite(uint8(Yellowim),'/home/samya/Desktop/Image_Pasting/ORMSWB/6/yellowbackground.jpg');
end

if(GreenFlag==1)
	colorshade='Green';
	predictedobject=predictshapelessname(objectid,colorshade);
	figure('Name',char(strcat(predictedobject,':',char(colorshade))));imshow(uint8(Greenim));
    %imwrite(uint8(Greenim),'/home/samya/Desktop/Image_Pasting/ORMSWB/6/greenbackground.jpg');
end

if(BlueFlag==1)
	colorshade='Blue';
	predictedobject=predictshapelessname(objectid,colorshade);
	figure('Name',char(strcat(predictedobject,':',char(colorshade))));imshow(uint8(Blueim));
    %imwrite(uint8(Blueim),'/home/samya/Desktop/Image_Pasting/ORMSWB/6/bluebackground.jpg');
end

if(VioletFlag==1)
	colorshade='Violet';
	predictedobject=predictshapelessname(objectid,colorshade);
	figure('Name',char(strcat(predictedobject,':',char(colorshade))));imshow(uint8(Violetim));
end

if(BrownFlag==1)
	colorshade='Brown';
	predictedobject=predictshapelessname(objectid,colorshade);
	figure('Name',char(strcat(predictedobject,':',char(colorshade))));imshow(uint8(Brownim));	
end

if(BlackFlag==1)
	colorshade='Black';		
	predictedobject=predictshapelessname(objectid,colorshade);
	figure('Name',char(strcat(predictedobject,':',char(colorshade))));imshow(uint8(Blackim));
end

if(GreyFlag==1)
	colorshade='Grey';
	predictedobject=predictshapelessname(objectid,colorshade);
	figure('Name',char(strcat(predictedobject,':',char(colorshade))));imshow(uint8(Greyim));
end

if(WhiteFlag==1)
	colorshade='White';
	predictedobject=predictshapelessname(objectid,colorshade);
	figure('Name',char(strcat(predictedobject,':',char(colorshade))));imshow(uint8(Whiteim));
end



figure('Name','All Boundaries');imshow(ucm);
figure('Name','Selected Boundaries');imshow(bdry);
figure('Name','Selected Regions with Random Colors');imshow(labels,[]);colormap(jet);

end