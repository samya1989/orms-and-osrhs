   %%%%%%%%%%%%%%%%%%%%%%%%%% START TESTING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   clear all;
   close all;
   clc;
      
   load('/home/samya/Desktop/Thesis_ODD/Duck/Pose2/MMCF.mat','MMCF');
   load('/home/samya/Desktop/Thesis_ODD/Duck/Pose2/Svm.mat','model');
   
   % addpath to the libsvm toolbox
   addpath('/home/samya/Desktop/libsvm-3.17/matlab');
   
   [filterlength filterwidth]=size(MMCF);
      
   testimagelength=600;
   testimagewidth=800;
   
   %%%%%%%%%%%%%%%%%%%%%%% TEST IMAGE PRE-PROCESSING %%%%%%%%%%%%%%%%%%%%%
   testim=imread('/home/samya/Desktop/test.jpg');
   
    [rows columns numberOfColorChannels] = size(testim);
    
    if (numberOfColorChannels > 1)
        testim=rgb2gray(testim);
    end
      
    testim=imresize(testim,[testimagelength testimagewidth]);
    figure;imshow(testim);
    
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
       
   %%%%%%%%%%%%%%%%%%%%%%%%    TESTing Starts    %%%%%%%%%%%%%%%%%%%%%%%%%%

   for zoom=100:-1:10
       
       resize=zoom/100
       testimdouble=imresize(testim,resize);
       [testimdoublelength testimdoublewidth]=size(testimdouble);
       
       imgnottransformed=testimdouble;
       testimdouble=double(testimdouble);
       
       
       %Making the test image zero mean and unit variance
       mean_val = mean(testimdouble(:));
       variance_val = var(testimdouble(:));
       
       if variance_val~=0
            testimdouble = (testimdouble-mean_val)/variance_val;
       end
       
       %Making the test image unit norm
       testimdouble = testimdouble/norm(testimdouble(:),2);
      
       result=xcorr2(testimdouble,MMCF);
       %figure;surf(result); 
       
       % Getting Local Maximas
       dilate=ones(2*filterlength-1,2*filterwidth-1);
       dilate(filterlength,filterwidth)=0;
       maximas = result > imdilate(result,dilate);

       
       % For each local maximas checking if the object is there
       
       [m n]=size(result);
       
       for i=1:m
           for j=1:n

               if(maximas(i,j)==1)
                   
                       % Now check if the image corresponding to the peak
                       % is in the class
                       
                       if((i-filterlength+1>0) & (j-filterwidth+1>0) & i<=testimdoublelength & j<=testimdoublewidth)
                            
                            subimg=imgnottransformed(i-filterlength+1:i,j-filterwidth+1:j);
                            
                            subimg=double(subimg);
                                  
                            %Making the test image zero mean and unit variance
                            mean_val = mean(subimg(:));
                            variance_val = var(subimg(:));
       
                            if variance_val~=0
                                subimg = (subimg-mean_val)/variance_val;
                            end
       
                            %Making the test image unit norm
                            subimg = subimg/norm(subimg(:),2);
                            
                            csvwrite('temp.csv',subimg(:).');
                            system('./convert.out temp.csv');
                            % read the test data set
                            [datalabel, testdata] = libsvmread(fullfile(pwd,'temp.csv_out'));
                            
                            [predict_label, accuracy, prob_values] = svmpredict([1], testdata, model, '-b 1');
                            
                            if(predict_label==1)
                                
                                subimg=imgnottransformed(i-filterlength+1:i,j-filterwidth+1:j);
                                prob_values(1)
                                figure;imshow(subimg);
                                
                            end
                       end
               end

           end
       end
       
   end