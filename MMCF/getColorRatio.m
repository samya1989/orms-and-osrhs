function [Red,Orange,Yellow,Green,Blue,Violet,Brown,Black,Grey,White] = getColorRatio(rgbimage)

    clear all;
    close all;
    clc;

    
    Red=0;
    Orange=0;
    Yellow=0;
    Green=0;
    Blue=0;
    Violet=0;
    Brown=0;
    Black=0;
    Grey=0;
    White=0;
    
    load('/home/samya/Desktop/colornamesshades.mat','names','shades','rgbhsl');
    
    rgbimage=imread('/home/samya/Desktop/eagles3.JPG');

    rgbimage=imresize(rgbimage,[15 20]);
    [m n l]=size(rgbimage);

    if(l==3)

        for i=1:m
            for j=1:n

                r=rgbimage(i,j,1);
                g=rgbimage(i,j,2);
                b=rgbimage(i,j,3);
                
                if r<16
                    r=dec2hex(r);
                    r=strcat('0',r);
                else
                    r=dec2hex(r);
                end
                
                if g<16
                    g=dec2hex(g);
                    g=strcat('0',g);
                else
                    g=dec2hex(g);
                end
                
                if b<16
                    b=dec2hex(b);
                    b=strcat('0',b);
                else
                    b=dec2hex(b);
                end
                
                colorcode=strcat('#',r,g,b);
                color=colorname(colorcode,names,shades,rgbhsl);

                if strcmp(color,'Red')==1
                    Red=Red+1;
                end
                if strcmp(color,'Orange')==1
                    Orange=Orange+1;
                end
                if strcmp(color,'Yellow')==1
                    Yellow=Yellow+1;
                end
                if strcmp(color,'Green')==1
                    Green=Green+1;
                end
                if strcmp(color,'Blue')==1
                    Blue=Blue+1;
                end      
                if strcmp(color,'Violet')==1
                    Violet=Violet+1;
                end
                if strcmp(color,'Brown')==1
                    Brown=Brown+1;
                end
                if strcmp(color,'Black')==1
                    Black=Black+1;
                end
                if strcmp(color,'Grey')==1
                    Grey=Grey+1;
                end
                if strcmp(color,'White')==1
                    White=White+1;
                end

            end
        end

        %Calculating Percentages
        Red=double(Red)/(m*n)*100
        Orange=double(Orange)/(m*n)*100
        Yellow=double(Yellow)/(m*n)*100
        Green=double(Green)/(m*n)*100
        Blue=double(Blue)/(m*n)*100
        Violet=double(Violet)/(m*n)*100
        Brown=double(Brown)/(m*n)*100
        Black=double(Black)/(m*n)*100
        Grey=double(Grey)/(m*n)*100
        White=double(White)/(m*n)*100

    end
    
end