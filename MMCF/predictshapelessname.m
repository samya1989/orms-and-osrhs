function[ predictedshapelessobjectname ]=predictshapelessname( objectid , colorname )

    db=fopen('/home/samya/Desktop/Thesis_ODD/objectsdb');
    objectsdb=textscan(db,'%d %s %s %d');
    fclose(db);
    
    objectoccurence=csvread('/home/samya/Desktop/Thesis_ODD/occurence');
    
    %objectid=1
    %colorname='Yellow'
    
    % Find all the shapeless objects of given color that co-occuered with
    % the objectid
    
    [m n]=size(objectoccurence);
    probabilities=zeros(length(objectsdb{1}),1);
    occurcount=0;
    
    for i=1:m
        
        flag=0;
        for j=1:n
            
            %Check if objectid occured in the transaction
            
            if objectoccurence(i,j)==objectid;
                occurcount=occurcount+1;
                flag=1;
            end
            
        end
        
        if flag==0
            continue;
        end
        
        for j=1:n
            
            if objectoccurence(i,j)==0
                
                break;
                
            end
            
            %Check if shapeless and of given color
            if objectsdb{4}(objectoccurence(i,j))==1 && strcmp((objectsdb{3}(objectoccurence(i,j))),colorname)==1
                
                probabilities(objectoccurence(i,j))=probabilities(objectoccurence(i,j))+1;
                
            end
            
        end
        
    end
    
    %probabilities
    
    [maxv pos]=max(probabilities);
    
    if sum(probabilities)==0
        predictedshapelessobjectname='Unknown shapeless object';
    else
        predictedshapelessobjectname=objectsdb{2}(pos);
    end

end