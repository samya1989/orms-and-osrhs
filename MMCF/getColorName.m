function [ colorname ] = getColorName(r,g,b)

    %This function, given R G B values, gives the color name, by calling a JAVA
    %Class which gets it done from javascript
    
    javaaddpath /home/samya/Desktop/MMCF/ColorTest
    colorcode=strcat(dec2hex(r),dec2hex(g),dec2hex(b));
    colorname = javaMethod('getColorName',ColorName,colorcode);
    
end