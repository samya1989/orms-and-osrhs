    positivepath='/home/samya/Desktop/Results/Duck/Pose1/';         %Takes file who are in the class i.e. 1
    negativepath='/home/samya/Desktop/Results/Duck/Pose2/';         %Takes file who are not in the class i.e. -1
    
    filterlength=60;
    filterwidth=80;
    paddedfilterlength=(2*filterlength*filterwidth-1);
    
    positiveimagefiles = [dir(strcat(positivepath,'*.ppm')) ; dir(strcat(positivepath,'*.gif'));  dir(strcat(positivepath,'*.jpg')); dir(strcat(positivepath,'*.png'))];
    % Number of positive files found
    positivefilescount = length(positiveimagefiles)
    
    negativeimagefiles = [dir(strcat(negativepath,'*.ppm'));  dir(strcat(negativepath,'*.gif'));  dir(strcat(negativepath,'*.jpg')); dir(strcat(negativepath,'*.png'))];
    % Number of negative files found
    negativefilescount = length(negativeimagefiles)
    
    allimagefiles = [positiveimagefiles ; negativeimagefiles];
    totalfilescount = length(allimagefiles)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    helloo='Done till image counting'
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    imagevectors=zeros(totalfilescount,filterlength*filterwidth);
    
    for go=1:totalfilescount
        
        if(go<=positivefilescount)
            path=positivepath;
        else
            path=negativepath;
        end
        
        currentfilename = allimagefiles(go).name;
        currentimage = imread(strcat(path,currentfilename));
        currentimage = imresize(currentimage,[filterlength filterwidth]);
        
        [rows columns numberOfColorChannels] = size(currentimage);
        if numberOfColorChannels > 1
            currentimage=rgb2gray(currentimage);
        end
        
        currentimage=double(currentimage);
        currentimage=currentimage(:);
        
        %Making it zero mean and unit variance
        mean_val = mean(currentimage);
        variance_val = var(currentimage);
        
        if variance_val~=0
            currentimage = (currentimage-mean_val)/variance_val;
        end
        
        %Making the image unit norm
        currentimage=currentimage/norm(currentimage,2);
        
        imagevectors(go,:)=currentimage.';
        
                          
    end
    
    positives=imagevectors(1:positivefilescount,:);
    negatives=imagevectors(positivefilescount+1:end,:);
    
    csvwrite('/home/samya/Desktop/Results/Duck/Pose1/images.csv',positives);
    csvwrite('/home/samya/Desktop/Results/Duck/Pose2/images.csv',negatives);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    helloo='CSV files prepared and written'
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
