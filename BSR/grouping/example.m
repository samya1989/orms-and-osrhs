clear all;
close all;
clc;
presentpath='/home/samya/Desktop/BSR/grouping/';

%% Compute globalPb and hierarchical segmentation for an example image.

addpath(fullfile(presentpath,'lib'));

I=imread(strcat(presentpath,'data/2.jpg'));
I=imresize(I,[300 400]);   
imwrite(I,strcat(presentpath,'data/wall.jpg'));

% %% 1. compute globalPb on a BSDS image (5Gb of RAM required)
% %clear all; close all; clc;
% 
% %presentpath='/home/samya/Desktop/BSR/grouping/';
% 
% imgFile = strcat(presentpath,'data/wall.jpg');
% outFile = strcat(presentpath,'data/wall_gPb.mat');
% 
% gPb_orient = globalPb(imgFile, outFile);
% 
% 
% %% 2. compute Hierarchical Regions
% 
% % for boundaries
% ucm = contours2ucm(gPb_orient, 'imageSize');
% imwrite(ucm,strcat(presentpath,'data/wall_ucm.bmp'));
% 
% % for regions 
% ucm2 = contours2ucm(gPb_orient, 'doubleSize');
% save(strcat(presentpath,'data/wall_ucm2.mat'),'ucm2');

%% 3. usage example
%clear all;close all;clc;

presentpath='/home/samya/Desktop/BSR/grouping/';

%load double sized ucm
load(strcat(presentpath,'data/wall_ucm2.mat'),'ucm2');

% convert ucm to the size of the original image
ucm = ucm2(3:2:end, 3:2:end);

% get the boundaries of segmentation at scale k in range [0 1]
k = 0.4;
bdry = (ucm >= k);

% get superpixels at scale k without boundaries, gives connected regions,
% will be used for shape matching

[labels2 uniformregioncount] = bwlabel(ucm2 <= k);
labels = labels2(2:2:end, 2:2:end);

%num has the number of connected components
connectedregions=uniformregioncount

effectiveuniformsegmentcount=0;
system(char(strcat('rm -rf',{' '},presentpath,'data/segments/*')));
fid=fopen(strcat(presentpath,'data/segments/details'),'w');
   
%select each regions
for i=1:uniformregioncount
    
    selectedlabel=(labels==i);
    selectedlabel=(selectedlabel~=1);
    
    if(sum(selectedlabel==0)<50)
       continue;
    end
    
    effectiveuniformsegmentcount=effectiveuniformsegmentcount+1;
    pgmcreate(presentpath,strcat('data/segments/',num2str(effectiveuniformsegmentcount),'.pgm'),selectedlabel);
        
    [selectedlabel complementregioncount]=bwlabel(selectedlabel);
    
    fprintf(fid,'%d %d\n',effectiveuniformsegmentcount,complementregioncount);
    
    for j=1:complementregioncount
       subselectedlabel=(selectedlabel==j);
       subselectedlabel=(subselectedlabel~=1);
       pgmcreate(presentpath,strcat('data/segments/',num2str(effectiveuniformsegmentcount),'_',num2str(j),'.pgm'),subselectedlabel);
        
    end
 
end

fclose(fid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %creating a pgm file of the region selected
% 
% fid=fopen(strcat(presentpath,'data/segment.pgm'),'w');
% fclose(fid);
% imwrite(labels,strcat(presentpath,'data/segment.pgm'),'pgm','Encoding','ASCII','MaxValue',255);
% labels1=imread(strcat(presentpath,'data/segment.pgm'));
% labels1=padarray(labels1,[1 1],255);
% 
% fid=fopen(strcat(presentpath,'data/segment.pgm'),'w');
% fprintf(fid,'%s\n%d %d\n%d\n','P2',(n+2),(m+2),255);
% fclose(fid);
% dlmwrite(strcat(presentpath,'data/segment.pgm'),labels1,'-append','delimiter',' ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;imshow(strcat(presentpath,'data/wall.jpg'));
figure;imshow(ucm);
figure;imshow(bdry);
figure;imshow(labels,[]);colormap(jet);

% %% 4. compute globalPb on a large image:
% 
% %clear all; close all; clc;
% 
% presentpath='/home/samya/Desktop/BSR/grouping/';
% 
% imgFile = strcat(presentpath,'data/101087_big.jpg');
% outFile = strcat(presentpath,'data/101087_big_gPb.mat');
% 
% gPb_orient = globalPb_pieces(imgFile, outFile);
% delete(outFile);
% figure; imshow(max(gPb_orient,[],3)); colormap(jet);


%% 5. See also:
%
%   grouping/run_bsds500.m for reproducing our results on the BSDS500  
%
%   interactive/example_interactive.m for interactive segmentation
%
%   bench/test_benchs.m for an example on using the BSDS500 benchmarks

