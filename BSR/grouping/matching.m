clear all
close all
clc

presentpath='/home/samya/Desktop/BSR/grouping/';

%% Compute globalPb and hierarchical segmentation for an example image.

addpath(fullfile(presentpath,'lib'));

I=imread(strcat(presentpath,'data/t.jpg'));
I=imresize(I,[300 400]);   
imwrite(I,strcat(presentpath,'data/wall.jpg'));
figure;imshow(strcat(presentpath,'data/wall.jpg'));

% %% 1. compute globalPb on a BSDS image (5Gb of RAM required)
% %clear all; close all; clc;
% 
% %presentpath='/home/samya/Desktop/BSR/grouping/';
% 
% imgFile = strcat(presentpath,'data/wall.jpg');
% outFile = strcat(presentpath,'data/wall_gPb.mat');
% 
% gPb_orient = globalPb(imgFile, outFile);
% 
% 
% %% 2. compute Hierarchical Regions
% 
% % for boundaries
% ucm = contours2ucm(gPb_orient, 'imageSize');
% imwrite(ucm,strcat(presentpath,'data/wall_ucm.bmp'));
% 
% % for regions 
% ucm2 = contours2ucm(gPb_orient, 'doubleSize');
% save(strcat(presentpath,'data/wall_ucm2.mat'),'ucm2');

%% 3. usage example
%clear all;close all;clc;

presentpath='/home/samya/Desktop/BSR/grouping/';

%load double sized ucm
load(strcat(presentpath,'data/wall_ucm2.mat'),'ucm2');

% convert ucm to the size of the original image
ucm = ucm2(3:2:end, 3:2:end);

% get the boundaries of segmentation at scale k in range [0 1]
k = 0.7;
bdry = (ucm >= k);

% get superpixels at scale k without boundaries, gives connected regions,
% will be used for shape matching

[labels2 uniformregioncount] = bwlabel(ucm2 <= k);
labels = labels2(2:2:end, 2:2:end);

%num has the number of connected components
connectedregions=uniformregioncount  

pixelsums=zeros(uniformregioncount,3);
pixelcountperregion=zeros(uniformregioncount,1);

%select each region and sum up color values 
for i=1:300
    for j=1:400
    
    pixelcountperregion(labels(i,j),1)=pixelcountperregion(labels(i,j),1)+1;
    
    for k=1:3
        
        pixelsums(labels(i,j),k)=pixelsums(labels(i,j),k)+double(I(i,j,k))/255;
        
    end
    
    end
end

pixelsavg=zeros(uniformregioncount,3);
%for each region find average
for i=1:uniformregioncount
    
    for j=1:3
        
        pixelsavg(i,j)=pixelsums(i,j)/pixelcountperregion(i,1);
        pixelsavg(i,j)=pixelsavg(i,j)*255;
    end
    
end 

pixelsavg=floor(pixelsavg);

for i=1:uniformregioncount
    
           
        r=pixelsavg(i,1);
        g=pixelsavg(i,2);
        b=pixelsavg(i,3);
                
        if r<16
            r=dec2hex(r);
            r=strcat('0',r);
        else
            r=dec2hex(r);
        end
                
        if g<16
            g=dec2hex(g);
            g=strcat('0',g);
        else
            g=dec2hex(g);
        end
                
        if b<16
            b=dec2hex(b);
            b=strcat('0',b);
        else
            b=dec2hex(b);
        end
                
        colorcode=strcat('#',r,g,b);
        colorshade=colorname(colorcode);
        
        fprintf('%d %s %s\n',i,char(colorcode),char(colorshade));
        
end   

% figure;imshow(ucm);
% figure;imshow(bdry);
% figure;imshow(labels,[]);colormap(jet);

% %% 4. compute globalPb on a large image:
% 
% %clear all; close all; clc;
% 
% presentpath='/home/samya/Desktop/BSR/grouping/';
% 
% imgFile = strcat(presentpath,'data/101087_big.jpg');
% outFile = strcat(presentpath,'data/101087_big_gPb.mat');
% 
% gPb_orient = globalPb_pieces(imgFile, outFile);
% delete(outFile);
% figure; imshow(max(gPb_orient,[],3)); colormap(jet);


%% 5. See also:
%
%   grouping/run_bsds500.m for reproducing our results on the BSDS500  
%
%   interactive/example_interactive.m for interactive segmentation
%
%   bench/test_benchs.m for an example on using the BSDS500 benchmarks

