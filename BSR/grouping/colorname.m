    %Needs color in #abcdef form
    function [colorshade]=colorname(color) 
        
            
%         load('/home/samya/Desktop/colornamesshades.mat','names','shades');
%         
%         rgbhsl=zeros(length(names),6);
%         for i=1:length(names)
%                  
%                 color=names(i,1);
%                 color=strcat('#',color);
%                 color=char(color);
%                 
%                 rgbvals = rgb(color);
%                 rgbhsl(i,1) = rgbvals(1);
%                 rgbhsl(i,2) = rgbvals(2);
%                 rgbhsl(i,3) = rgbvals(3);
%                 
%                 hslvals = hsl(color);
%                 rgbhsl(i,4) = hslvals(1);
%                 rgbhsl(i,5) = hslvals(2); 
%                 rgbhsl(i,6) = hslvals(3);
%                 
%         end
%         
%         save('/home/samya/Desktop/colornamesshades.mat','names','shades','rgbhsl');
        load('/home/samya/Desktop/MMCF/colornamesshades.mat','names','shades','rgbhsl');
        
        %color='#23ef56';
        color=char(color);
        color = upper(color);
        
        rgbvals = rgb(color);
        r = rgbvals(1);
        g = rgbvals(2);
        b = rgbvals(3);
        
        hslvals = hsl(color);
        h = hslvals(1);
        s = hslvals(2); 
        l = hslvals(3);
        
        ndf1 = 0; 
        ndf2 = 0; 
        ndf = 0;
        cl = -1;
        df = -1;

        for i = 1:length(names)
        
            if(strcmp(color,strcat('#',names(i,1)))==1)
        
                colorshade=names(i,3);
                return;
            end
          
            ndf1 = ((r - rgbhsl(i,1))^2) + ((g - rgbhsl(i,2))^2) + ((b - rgbhsl(i,3))^2);
            ndf2 = abs((h - rgbhsl(i,4))^2) + (s - rgbhsl(i,5))^2 + abs((l - rgbhsl(i,6))^2);
            ndf = ndf1 + ndf2 * 2;
            if(df < 0 || df > ndf)
        
                df = ndf;
                cl = i;
            end
        end

        colorshade=names(cl,3);
        
   end

  % adopted from: Farbtastic 1.2
  % http://acko.net/dev/farbtastic
  function[hslvalues]=hsl(color)

    rgb = [ hex2dec(color(2:3))/255 hex2dec(color(4:5))/255 hex2dec(color(6:7))/255 ];
    r = rgb(1);
    g = rgb(2); 
    b = rgb(3);

    mini = min(r, min(g, b));
    maxi = max(r, max(g, b));
    delta = maxi - mini;
    l = (mini + maxi) / 2;

    s = 0;
    if(l > 0 && l < 1)
      
      value=2-2*l;
      if l<0.5
          value=2*l;
      end
      s = delta / value;
      
    end
    
    h = 0;
    if(delta > 0)
    
      if (maxi == r && maxi ~= g) 
            h =h+ (g - b) / delta;
      end
      if (maxi == g && maxi ~= b) 
            h = h+(2 + (b - r) / delta);
      end
      if (maxi == b && maxi ~= r) 
            h = h+(4 + (r - g) / delta);
      end
      h = h/6;
    end
    hslvalues=[floor(h * 255) floor(s * 255) floor(l * 255)];
  end

  % adopted from: Farbtastic 1.2
  % http://acko.net/dev/farbtastic
  %Here color is of form #ffffff
  function [colorcode]=rgb(color) 
        
        colorcode=[ hex2dec(color(2:3)) hex2dec(color(4:5)) hex2dec(color(6:7)) ];
        
  end

%   function[colorcode]=shadergb(shadename) 
%         
%         colorcode='#000000';
%         for i = 0:length(ntc.shades) 
%         
%           if(shadename == ntc.shades[i][1])
%             colorcode=strcat( '#',ntc.shades[i][0]);
%           end
%           
%         end
%       
%   end
