function [ done ] = pgmcreate(presentpath,filename,selectedlabel)

    %creating a pgm file of the region selected

    [m n]=size(selectedlabel);
    fid=fopen(strcat(presentpath,filename),'w');
    fclose(fid);
    imwrite(selectedlabel,strcat(presentpath,filename),'pgm','Encoding','ASCII','MaxValue',255);
    selectedlabel=imread(strcat(presentpath,filename));
    selectedlabel=padarray(selectedlabel,[1 1],255);

    fid=fopen(strcat(presentpath,filename),'w');
    fprintf(fid,'%s\n%d %d\n%d\n','P2',(n+2),(m+2),255);
    fclose(fid);
    dlmwrite(strcat(presentpath,filename),selectedlabel,'-append','delimiter',' ');
    I=imread(strcat(presentpath,filename));
    imwrite(I,strcat(presentpath,filename,'.jpg'),'jpg')

end