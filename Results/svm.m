clear
clc
close all

path='/home/samya/Desktop/Thesis_ODD/Eagle/Pose3';

% addpath to the libsvm toolbox
addpath('/home/samya/Desktop/libsvm-3.17/matlab');

% addpath to the data
dirData = path; 
addpath(dirData);

% read the data set
[labels, data] = libsvmread(fullfile(dirData,'total.combined'));
[N D] = size(data);  
   
% Train the SVM
model = svmtrain(labels, data, '-s 0 -t 2 -c 2048 -g 0.00048828125 -b 1');
save(strcat(path,'/Svm.mat'),'model');